# Discord Calendar bot

![Picture](https://shields.io/gitlab/pipeline-status/Come-Wasik/discord-bot-pgsql-template?branch=master)

## Installation

### Developement

+ Setup your environment variable by creating the .env file by running `cp .env.example .env`
+ Set your variables into the .env file
+ Build the Dockerfile byu running `docker-compose build`
+ Start the bot by running `docker-compose up -d`

### Production

+ Setup your environment variable by creating the .env file by running `cp .env.example .env`
+ Set your variables into the .env file
+ Build the Dockerfile byu running `docker-compose build`
+ Start the bot by running `docker-compose -f production.yml up -d`

## Command list

Update the command list:

+ Run `docker-compose exec nodejs npm run commands:update`

Update the database model list:

+ Run `docker-compose exec nodejs npm run db:seed`

Build the typescript files:

+ Run `docker-compose exec nodejs npm run build`

## Manage commands

All the commands are prefixed by a symbol that is described in the src/config.json file.
Write !help to show all the commands
