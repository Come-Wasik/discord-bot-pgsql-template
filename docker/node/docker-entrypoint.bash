#!/bin/sh
set -e

echo "Entrypoint runned"

# Run an installation of packages
if [ "$NODE_ENV" == 'production' ]; then
    npm install --production
else
    npm install -D
fi

echo "Waiting for the laravel database to start on host '$POSTGRES_HOST' and port '$POSTGRES_PORT'"
ATTEMPTS_LEFT_TO_REACH_DATABASE=60

until nc -z $POSTGRES_HOST $POSTGRES_PORT; do
    if [ $ATTEMPTS_LEFT_TO_REACH_DATABASE -eq 0 ]; then
        echo "The postgresql database is not reachable on host '$POSTGRES_HOST' and port '$POSTGRES_PORT'"
        exit 1
    fi
    ATTEMPTS_LEFT_TO_REACH_DATABASE=$((ATTEMPTS_LEFT_TO_REACH_DATABASE-1))
    echo "Didn't reach. Try again..."
    sleep 1
done
echo "The database is now ready and reachable"

# Run command with node if the first argument contains a "-" or is not a system command. The last
# part inside the "{}" is a workaround for the following bug in ash/dash:
# https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=874264
if [ "${1#-}" != "${1}" ] || [ -z "$(command -v "${1}")" ] || { [ -f "${1}" ] && ! [ -x "${1}" ]; }; then

    if [ "$NODE_ENV" == 'production' ]; then
        npm run db:seed
    fi
	set -- npm run "$@"
fi

# Run the command
exec "$@"
