// Require the necessary discord.js classes
const { Client, GatewayIntentBits } = require('discord.js');
const config = require('./helpers/appConfig');

// Create a new client instance
const client = new Client({
	intents: [
		GatewayIntentBits.Guilds,
		GatewayIntentBits.GuildMessages,
		GatewayIntentBits.DirectMessages,
		GatewayIntentBits.MessageContent,
	],
});
// Gets the main bot file
const { registerBotActions } = require('./bot/main');
registerBotActions(client);

// Login to Discord with your client's token
client.login(config.get('DISCORD_BOT_TOKEN'));
