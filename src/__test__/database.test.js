/* eslint-disable no-undef */
const { Sequelize } = require("sequelize");
const sequelize = require("../database/connexion");

describe('Database checks', () => {
	beforeAll(async () => {
		sequelize.authenticate();
		const err = await require('../scripts/run-models');
		if (err instanceof Error) {
			throw err;
		}
	});

	afterAll(async () => {
		await sequelize.close();
	});

	test('check the database connexion', (async () => {
		expect(sequelize instanceof Sequelize).toBeTruthy();

		sequelize.authenticate();
	}));
});
