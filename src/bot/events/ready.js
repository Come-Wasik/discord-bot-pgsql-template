const config = require('../../helpers/appConfig');

module.exports = {
	name: 'ready',
	once: true,
	/**
	 * Execute the event listener
	 * @param {Client} client Our discordjs client
	 */
	async execute(client) {
		console.log(`Wynter is comming! My name is ${client.user.tag}`);
		client.user.setActivity('Freeze EVERYTHING');

		const botChannel = await client.channels.fetch(config.get('DISCORD_BOT_CHANNEL_ID'));
		// Example message
		await botChannel.send("Hello ! I'm welcome to meet you");
	},
};
