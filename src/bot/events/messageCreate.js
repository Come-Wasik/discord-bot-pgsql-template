// eslint-disable-next-line no-unused-vars
const { Message } = require("discord.js");
const { globPromise } = require("../../helpers/betterglob");
const config = require('../../helpers/appConfig');

const botCommandStack = {
	prefix: config.get('COMMANDS_PREFIX'),
	items: [],
};

module.exports = {
	name: 'messageCreate',
	/**
	 * Execute the event listener
	 * @param {Message} message A discord message object
	 */
	async execute(message) {
		// Assert the message author is not a bot
		if (message.author.bot) return;

		// Assert the message content use the defined prefix
		if (!message.content.startsWith(botCommandStack.prefix)) return;

		// Refresh custom commands
		await globPromise("bot/commands/custom/**/*.js", (file) => {
			const command = require("../../" + file);
			if (!botCommandStack.items.some((obj) => obj.content === command.content)) {
				botCommandStack.items.push(command);
			}
		});

		// Loop over each commands
		for (const botCommand of botCommandStack.items) {

			if (typeof botCommand.content !== "string") {
				throw Error("The command action must be a string.");
			}
			const botCommandParts = botCommand.content.split(' ');
			const userCommandParts = message.content.split(' ');
			const userCommandArgs = [];
			let isOurActiveCommand = true;

			// If the typed command has different word size, pass to the next command
			if (botCommandParts.length !== userCommandParts.length) {
				continue;
			}

			// Check each part of the commands
			for (let index = 0; index < userCommandParts.length; index++) {
				const botPart = botCommandParts[index];
				let userPart = userCommandParts[index];

				// Remove prefix from comparaison
				if (index === 0) {
					userPart = userPart.slice(1);
				}

				// The current part is maybe a replacing content
				if (/^\{.+\}/.test(botPart)) {
					// It is, so take the name of the part and add it to the command args
					userCommandArgs.push(userPart);
				} else if (botPart !== userPart) {
					// Compare each part. If any part is different, this is not our searched command
					isOurActiveCommand = false;
					userCommandArgs.length = 0;
				}
			}

			if (isOurActiveCommand) {
				if (typeof botCommand.action !== "function") {
					throw Error("The command action must be a function.");
				}
				botCommand.action(message, ...userCommandArgs);
				return;
			}
		}

		// Add the help command that depends on the others commands
		if (message.content === botCommandStack.prefix + "help") {
			let replyText = '**Command list:**\n';
			botCommandStack.items.forEach(command => {
				replyText += `${botCommandStack.prefix}${command.content}\n`;
				if (command.description) {
					replyText += `\t*${command.description}*\n`;
				}
			});
			message.reply(replyText);
		}
	},
};
