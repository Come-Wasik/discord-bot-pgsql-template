const { Collection } = require('discord.js');
const { globPromise } = require('../helpers/betterglob');

module.exports = {
	/**
	 * Register the discord bot actions
	 * @param {Client} client Our discordjs client
	 */
	registerBotActions: async function registerBotActions(client) {
		// This will contain all our commands
		client.slashCommands = new Collection();

		// This will load all our commands (blocking action)
		await globPromise("bot/commands/slash/**/*.js", filePath => {
			const command = require("../" + filePath);
			client.slashCommands.set(command.data.name, command);
		});

		// This will load all our events (blocking action)
		await globPromise("bot/events/*.js", filePath => {
			const event = require("../" + filePath);
			if (event.once) {
				client.once(event.name, (...args) => event.execute(...args));
			} else {
				client.on(event.name, (...args) => event.execute(...args));
			}
		});
	},
};
