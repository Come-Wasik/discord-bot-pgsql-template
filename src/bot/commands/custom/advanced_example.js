module.exports = {
	content: 'hello {any_word}',
	description: 'The bot say hello to any single word you entered',
	/**
	 * Action when the command is triggered
	 * @param {Message} interaction A discord message object
	 */
	action: async function (interaction, any_word) {
		// In fact, any_word can be renamed because only the arguments order is important
		await interaction.reply(`Hello ${any_word}`);
	},
};
