module.exports = {
	content: 'hello',
	description: 'The bot say hello to you',
	/**
	 * Action when the command is triggered
	 * @param {Message} interaction A discord message object
	 */
	action: async function (interaction) {
		await interaction.reply(`Hello ${interaction.author.username}`);
	},
};
