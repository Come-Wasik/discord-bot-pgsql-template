const { SlashCommandBuilder } = require('discord.js');

const answers = [
	"Winter is coming.",
	"I'm the sky",
	"Try to approch me, you'll know the cold of my breath.",
];

module.exports = {
	data: new SlashCommandBuilder()
		.setName('juggernaut')
		.setDescription('Have a random Juggernaut advice'),
	async execute(interaction) {
		const randomAnswer = answers[Math.floor(Math.random() * answers.length)];
		await interaction.reply(randomAnswer);
	},
};
