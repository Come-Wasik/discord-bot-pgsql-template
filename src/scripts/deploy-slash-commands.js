const { Routes } = require('discord.js');
const { REST } = require('@discordjs/rest');
const config = require('../helpers/appConfig');
const { globPromise } = require("../helpers/betterglob");

// Run the following commands immediatly and asynchronly
(async function () {
	const slashCommands = [];

	// Will search and register our commands
	await globPromise(__dirname + "/../bot/commands/slash/**/*.js", (file) => {
		const command = require(file);
		slashCommands.push(command.data.toJSON());
	});

	// Will communicate with the discord server/guild then register new commands
	const rest = new REST({ version: '10' }).setToken(config.get('DISCORD_BOT_TOKEN'));

	rest.put(Routes.applicationGuildCommands(config.get('DISCORD_CLIENT_ID'), config.get('DISCORD_GUILD_ID')), { body: slashCommands })
		.then(data => console.log(`Successfully registered ${data.length} application commands.`))
		.catch(console.error);

})();
