const { globPromise } = require('../helpers/betterglob');

module.exports = new Promise(function (resolve, reject) {
	globPromise(__dirname + "/../database/models/*.js", async (file) => {
		console.log("Init model at path : " + file);
		const model = require(file);

		// Create the table if does not exists
		await model.sync({ force: true });
	})
		.then(() => {
			resolve(true);
		})
		.catch(err => {
			reject(err);
		});
});
