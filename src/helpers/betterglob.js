const glob = require("glob");

module.exports = {
	globPromise: function (path, todo) {
		return new Promise(function (resolve, reject) {
			try {
				glob(path, (err, files) => {
					if (err) {
						console.error(err);
						throw "At fatal error has occured for glob promise with path '" + path + "'.";
					} else {
						// If there is no files, stop the function
						if (files.length === 0) {
							resolve(true);
							return;
						}

						// Read file per file then execute todo
						files.forEach(async (file, index, items) => {
							await todo(file, index, items);

							// Indicate the function has reached the last file
							if (index + 1 === files.length) {
								resolve(true);
							}
						});
					}
				});
			} catch (err) {
				reject(err);
			}
		});
	},
};
