const configData = require('../config.json');
// Overload config file with env variables
for (const key in process.env) {
	if (Object.hasOwnProperty.call(process.env, key)) {
		const item = process.env[key];
		configData[key] = item;
	}
}

module.exports = {
	get: function (key) {
		return configData[key] ?? undefined;
	},
	set: function (key, data) {
		configData[key] = data;
	},
};
