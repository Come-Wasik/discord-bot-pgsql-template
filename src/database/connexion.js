const { Sequelize } = require('sequelize');
const Config = require('../helpers/appConfig');

// Get envs and default values
const postgres_host = Config.get('POSTGRES_HOST');
const postgres_db = Config.get('POSTGRES_DB');
const postgres_user = Config.get('POSTGRES_USER');
const postgres_pass = Config.get('POSTGRES_PASS');
const postgres_port = Config.get('POSTGRES_PORT');
const node_env = Config.get('NODE_ENV');

// Create new sequilize connexion
module.exports = new Sequelize(`postgres://${postgres_user}:${postgres_pass}@${postgres_host}:${postgres_port}/${postgres_db}`, {
	logging: node_env !== 'production',
});
