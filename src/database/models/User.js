const sequelize = require('../connexion');
const { DataTypes, Model } = require('sequelize');

class User extends Model { }

User.init({
	id: {
		type: DataTypes.INTEGER,
		autoIncrement: true,
		primaryKey: true,
	},
	username: {
		type: DataTypes.STRING,
		allowNull: false,
	},
	age: {
		type: DataTypes.INTEGER,
		allowNull: false,
	},
}, {
	sequelize,
	modelName: 'User',
});

// `sequelize.define` also returns the model
// console.log(User === sequelize.models.User); // true

module.exports = User;